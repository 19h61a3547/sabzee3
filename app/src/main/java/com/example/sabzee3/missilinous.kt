package com.example.sabzee3

data class Purchase(
    val id:String?,
    val name: String,
    val subname: String,
    val userQuantity: Int,
    val totalCost: Int,
    val userEmail: String,
    var adminAccepted: Boolean,
    val adminCancelled: Boolean,
    val date: String
)

data class Vegetables(
    val name: String,
    val subname: String,
    val quantity: Int,
    val cost: Int

)

data class LVegitables(
    val name: String,
    val types: List<String>
)