package com.example.sabzee3.user.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.sabzee3.R
import com.example.sabzee3.Vegetables
import com.example.sabzee3.databinding.HomeCardViewBinding
import com.example.sabzee3.user.viewmodel.UserViewModel

class MainScreenAdaptor(val viewModel: UserViewModel): ListAdapter<Vegetables, MainScreenAdaptor.ViewHolder>(DiffCallBack) {

    companion object DiffCallBack:DiffUtil.ItemCallback<Vegetables>(){
        override fun areItemsTheSame(oldItem: Vegetables, newItem: Vegetables): Boolean {
            return oldItem.name==newItem.name
        }

        override fun areContentsTheSame(oldItem: Vegetables, newItem: Vegetables): Boolean {
            return oldItem==newItem
        }

    }

    class ViewHolder(val binding: HomeCardViewBinding,val viewModel: UserViewModel): RecyclerView.ViewHolder(binding.root){
        fun bind(item: Vegetables){
            binding.vegiNameHome.text = item.name
            binding.vegiSubNameHome.text = item.subname
            binding.vegiCostHome.text = "Cost : ${item.cost}"
            binding.vegiQuantityHome.text = "Quantity: ${item.quantity}"
            binding.constraintLayout.setOnClickListener {
                viewModel.getSelectedPurchae(item)
                it.findNavController().navigate(R.id.action_userHomeFragment_to_purchaseFragment)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(HomeCardViewBinding.inflate(LayoutInflater.from(parent.context)),viewModel)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}