package com.example.sabzee3.user.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sabzee3.Purchase
import com.example.sabzee3.R
import com.example.sabzee3.databinding.RequestTileUserBinding
import com.example.sabzee3.user.viewmodel.UserViewModel

class MainScreenRequestsAdaptor(val viewModel: UserViewModel): ListAdapter<Purchase, MainScreenRequestsAdaptor.ViewHolder>(DiffCallBack) {

    companion object DiffCallBack: DiffUtil.ItemCallback<Purchase>(){
        override fun areItemsTheSame(oldItem: Purchase, newItem: Purchase): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Purchase, newItem: Purchase): Boolean {
            return oldItem==newItem
        }
    }

    class ViewHolder(val binding: RequestTileUserBinding,val viewModel: UserViewModel): RecyclerView.ViewHolder(binding.root){
        fun bind(item: Purchase){
            binding.purchaseVegiName.text = item.name
            binding.purchaseVegiSubName.text = item.subname
            binding.purchaseCostUser.text = item.totalCost.toString()
            binding.purchaseQuantityUser.text = item.userQuantity.toString()
            binding.purchaseDate.text = item.date
            binding.payment.visibility = View.GONE
            if(item.adminAccepted){
                binding.payment.visibility = View.VISIBLE
            }
            binding.cancel.setOnClickListener {
                viewModel.deleteTheRequest(item.id!!)
//                it.findNavController().popBackStack(R.id.userHomeFragment,false)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MainScreenRequestsAdaptor.ViewHolder {
        return ViewHolder(RequestTileUserBinding.inflate(LayoutInflater.from(parent.context)),viewModel)
    }

    override fun onBindViewHolder(holder: MainScreenRequestsAdaptor.ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}