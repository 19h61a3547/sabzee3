package com.example.sabzee3.user.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.sabzee3.Purchase
import com.example.sabzee3.Vegetables
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class UserViewModel(application: Application): AndroidViewModel(application) {

    private val _listOfVegetablesCanOrder = MutableLiveData<List<Vegetables>>()
    val listOfVegetablesCanOrder:LiveData<List<Vegetables>> = _listOfVegetablesCanOrder

    private val _requestedVegetables = MutableLiveData<List<Purchase>>()
    val requestedVegetables: LiveData<List<Purchase>> = _requestedVegetables

    private val _vegetables = Firebase.firestore.collection("vegetables")
    private val _request = Firebase.firestore.collection("requests")
    private val auth = Firebase.auth

    private var selectedPerchaseItem = Vegetables("",",",0,0)

    init {
        _vegetables.addSnapshotListener { snapshot, error ->
            if (error != null) {
                Log.w("UserViewModel", "Listen failed.", error)
                return@addSnapshotListener
            }
            if (snapshot != null && !snapshot.isEmpty) {
                val mArray = ArrayList<Vegetables>()
                for (p in snapshot) {
                    mArray.add(
                        Vegetables(
                            p.data["name"] as String, p.data["subname"] as String,
                            p.data["quantity"].toString().toInt() , p.data["cost"].toString().toInt()
                        )
                    )
                    Log.d("viewModel","Sucess + ${mArray.size}")
                }
                _listOfVegetablesCanOrder.value= mArray
            }
        }

        _request.addSnapshotListener { snapshot, error ->
            if (error != null) {
                Log.w("UserViewModel", "Listen failed.", error)
                return@addSnapshotListener
            }
            if (snapshot != null && !snapshot.isEmpty) {
                val mArray = ArrayList<Purchase>()
                for (p in snapshot) {
                    if(p.get("userEmail")==auth.currentUser?.email){
                        mArray.add(
                            Purchase(p.id,
                                p.data["name"] as String, p.data["subname"] as String,
                                p.data["userQuantity"].toString().toInt() , p.data["totalCost"].toString().toInt(),
                                p.data["userEmail"] as String, p.data["adminAccepted"] as Boolean,
                                p.data["adminCancelled"] as Boolean,p.data["date"] as String,
                            )
                        )
                    }
                    Log.d("viewModel","Sucess + ${mArray.size}")
                }
                _requestedVegetables.value= mArray
            }
        }

    }

    fun getSelectedPurchae(item: Vegetables){
        selectedPerchaseItem=item
    }

    fun setSelectedPurchase(): Vegetables{
        return selectedPerchaseItem
    }

    fun deleteTheRequest(id: String){
        _request.document(id).delete()

    }

    fun savingTheRequest(p: Purchase){
        _request.add(p)
    }
    fun doingPayment(){

    }
}