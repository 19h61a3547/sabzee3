package com.example.sabzee3.admin.viewmodel

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.sabzee3.LVegitables
import com.example.sabzee3.Purchase
import com.example.sabzee3.Vegetables
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class AdminViewModel(application: Application): AndroidViewModel(application) {

    val listOfVegitables = listOf<LVegitables>(
        LVegitables("Asparagus", listOf("Green","White","Purple")),
        LVegitables(
                "Avocado", listOf("Hass","Fuerte","Sharwill")),
        LVegitables("Bean", listOf("Green","Butter beans","Runner beans","Broad beans")),
        LVegitables(
                "Beetroot", listOf("Commom Beetroot")),
        LVegitables("Broccoli", listOf("Cathedral","Common")),
        LVegitables("Brussels sprouts", listOf("Common Brussels sprouts")),
        LVegitables("Cabbage", listOf("Green/Common","Savoy","Chinese","Red")),
        LVegitables("Capsicum", listOf("Green","Red","Yellow","Orange","Black/Purple"))
    )
    private val _listOfSubVegitables = MutableLiveData<List<String>>()
    val listOfSubVegitables:LiveData<List<String>> = _listOfSubVegitables

    private val _listOfVegetablesCanOrder = MutableLiveData<List<Vegetables>>()
    val listOfVegetablesCanOrder: LiveData<List<Vegetables>> = _listOfVegetablesCanOrder

    private val _requestedVegetables = MutableLiveData<List<Purchase>>()
    val requestVegetables: LiveData<List<Purchase>> = _requestedVegetables

    private var _vegiId =0
    private var _vegiSubId = 0

//    private var _requests = MutableLiveData<Boolean>(true)
//    val requests:LiveData<Boolean> = _requests

    private var _vegiSaved = MutableLiveData<Boolean>(false)
    val vegiSaved : LiveData<Boolean> = _vegiSaved

    private var _database : DatabaseReference = Firebase.database.reference
    private var _vegetables = Firebase.firestore.collection("vegetables")
    private var _request = Firebase.firestore.collection("requests")



    fun getVegiId(pos: Int){
        _vegiId = pos
        _listOfSubVegitables.value = listOfVegitables[pos].types
    }

    fun getVegiSubId(pos:Int){
        _vegiSubId=pos
    }

    fun vegiName(): String{
        return listOfVegitables[_vegiId].name
    }

    fun vegiSubName(): String{
        return listOfVegitables[_vegiId].types[_vegiSubId]
    }

    fun getAllotedVegetablesDataFromFirestore(){
        Log.d("ViewModel", "onSuccess: function")
        _vegetables.addSnapshotListener { snapshot, error ->
            if (error != null) {
                Log.w("UserViewModel", "Listen failed.", error)
                return@addSnapshotListener
            }
            if (snapshot != null && !snapshot.isEmpty) {
                val mArray = ArrayList<Vegetables>()
                for (p in snapshot) {
                    mArray.add(Vegetables(
                        p.data["name"] as String, p.data["subname"] as String,
                        p.data["quantity"].toString().toInt(), p.data["cost"].toString().toInt()
                    ))
                    Log.d("viewModel","Sucess + ${mArray.size}")
                }
                _listOfVegetablesCanOrder.value= mArray
            }
        }
    }

    fun getRequestsFromFirestore(){
        _request.addSnapshotListener { snapshot, error ->
            if (error != null) {
                Log.w("UserViewModel", "Listen failed.", error)
                return@addSnapshotListener
            }
            if (snapshot != null && !snapshot.isEmpty) {
                val mArray = ArrayList<Purchase>()
                for (p in snapshot) {
                    if(p.get("userEmail").toString()!="abc@cvsr.ac.in"){
                        mArray.add(
                            Purchase(p.id,
                                p.data["name"] as String, p.data["subname"] as String,
                                p.data["userQuantity"].toString().toInt() , p.data["totalCost"].toString().toInt(),
                                p.data["userEmail"] as String, p.data["adminAccepted"] as Boolean,
                                p.data["adminCancelled"] as Boolean,p.data["date"] as String,
                            )
                        )
                    }
                    Log.d("viewModel","Success + ${mArray.size}")
                }
                _requestedVegetables.value= mArray
            }
        }
    }

    fun saveToFirestore(item:Vegetables){
        _vegetables.document(item.name+"-"+item.subname).set(item)
            .addOnSuccessListener {
                _vegiSaved.value = true
            }.addOnCanceledListener {
                _vegiSaved.value = false
            }
    }

    fun deleteTheRequest(id: String){
        _request.document(id).delete()
    }

    fun changeThePurchaseBoolean(p: Purchase){
        _request.document(p.id!!).set(p)
    }

//    fun getRequests(){
//        _requests.value =true
//    }
//
//    fun flaseRequests(){
//        _requests.value = false
//    }

}