package com.example.sabzee3.admin.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sabzee3.Purchase
import com.example.sabzee3.R
import com.example.sabzee3.admin.viewmodel.AdminViewModel
import com.example.sabzee3.databinding.RequestTitleAdminBinding

class HomeRequestAdaptor(val viewModel: AdminViewModel): ListAdapter<Purchase , HomeRequestAdaptor.ViewHolder>(DiffCallBack) {

    companion object DiffCallBack: DiffUtil.ItemCallback<Purchase>(){
        override fun areItemsTheSame(oldItem: Purchase, newItem: Purchase): Boolean {
            return oldItem.name==newItem.name
        }

        override fun areContentsTheSame(oldItem: Purchase, newItem: Purchase): Boolean {
            return oldItem==newItem
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): HomeRequestAdaptor.ViewHolder {
        return ViewHolder(RequestTitleAdminBinding.inflate(LayoutInflater.from(parent.context)),viewModel)
    }

    override fun onBindViewHolder(holder: HomeRequestAdaptor.ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder(val binding: RequestTitleAdminBinding,val viewModel: AdminViewModel): RecyclerView.ViewHolder(binding.root){
        fun bind(item: Purchase){
            binding.vegiNameAdmin.text = item.name
            binding.subNameAdmin.text = item.subname
            binding.userPurchaseDate.text = "Date: "+item.date
            binding.paymentAgree.text = "Rs:"+item.totalCost.toString()
            binding.userQuantity.text = "Quantity: "+item.userQuantity.toString()
            binding.userEmail.text = item.userEmail

            binding.cancelAdmin.setOnClickListener {
                viewModel.deleteTheRequest(item.id!!)
            }

            if(item.adminAccepted){
                binding.cancelAdmin.isClickable = false
                binding.cancelAdmin.setBackgroundColor(Color.GRAY)
                binding.paymentAgree.isClickable = false
                binding.paymentAgree.setBackgroundColor(Color.GRAY)
            }


            binding.paymentAgree.setOnClickListener {
                item.adminAccepted = true
                viewModel.changeThePurchaseBoolean(item)
            }
        }
    }
}