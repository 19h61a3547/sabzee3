package com.example.sabzee3.admin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sabzee3.R
import com.example.sabzee3.Vegetables
import com.example.sabzee3.admin.adapters.AddAdaptor
import com.example.sabzee3.admin.adapters.HomeAdaptor
import com.example.sabzee3.admin.adapters.HomeRequestAdaptor
import com.example.sabzee3.admin.viewmodel.AdminViewModel
import com.example.sabzee3.databinding.FragmentAdminHomeBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AdminHomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AdminHomeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentAdminHomeBinding
    private lateinit var viewModel: AdminViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentAdminHomeBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(requireActivity()).get(AdminViewModel::class.java)
        viewModel.getAllotedVegetablesDataFromFirestore()
        viewModel.getRequestsFromFirestore()
        binding.recyclerView2.visibility = View.INVISIBLE
        binding.refresh.text = "All Vegetables"

        binding.floatingActionButton.setOnClickListener {
            it.findNavController().navigate(R.id.action_adminHomeFragment_to_addItemFragment)
        }

        binding.refresh.setOnClickListener {
            val v=binding.refresh.text
            if(v=="All Requests"){
                binding.requestRecycleView.visibility=View.VISIBLE
                binding.recyclerView2.visibility = View.INVISIBLE
                binding.textView5.visibility = View.GONE
                binding.refresh.text = "All Vegetables"
            }else{
                binding.requestRecycleView.visibility=View.INVISIBLE
                binding.recyclerView2.visibility = View.VISIBLE
                binding.textView5.visibility = View.GONE
                binding.refresh.text = "All Requests"
            }
        }

        viewModel.listOfVegetablesCanOrder.observe(viewLifecycleOwner, Observer {
            it?.let {
                binding.textView5.visibility = View.GONE
                val adaptor = HomeAdaptor()
                adaptor.submitList(it)
                binding.recyclerView2.adapter =adaptor
            }
        })

        viewModel.requestVegetables.observe(viewLifecycleOwner, Observer {
            it?.let{
                if(it.isEmpty()){
                    binding.textView5.visibility = View.VISIBLE
                    binding.requestRecycleView.visibility = View.INVISIBLE
                }else {
                    binding.textView5.visibility = View.GONE
                    val adaptor = HomeRequestAdaptor(viewModel)
                    adaptor.submitList(it)
                    binding.requestRecycleView.adapter = adaptor
                }
            }
        })

        binding.recyclerView2.layoutManager= GridLayoutManager(context,1)
        binding.requestRecycleView.layoutManager = GridLayoutManager(context,1)

        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AdminHomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AdminHomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}