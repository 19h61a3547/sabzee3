package com.example.sabzee3.admin.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sabzee3.LVegitables
import com.example.sabzee3.R
import com.example.sabzee3.admin.viewmodel.AdminViewModel
import com.example.sabzee3.databinding.VegiCardViewBinding

class AddAdaptor(private val viewModel: AdminViewModel): ListAdapter<LVegitables, AddAdaptor.ViewHolder>(DiffCallBack)

{

    companion object DiffCallBack:DiffUtil.ItemCallback<LVegitables>(){
        override fun areItemsTheSame(oldItem: LVegitables, newItem: LVegitables): Boolean {
            return oldItem.name==newItem.name
        }

        override fun areContentsTheSame(oldItem: LVegitables, newItem: LVegitables): Boolean {
            return oldItem==newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddAdaptor.ViewHolder {
        return ViewHolder(VegiCardViewBinding.inflate(LayoutInflater.from(parent.context)),viewModel)
    }

    override fun onBindViewHolder(holder: AddAdaptor.ViewHolder, position: Int) {
        holder.bind(getItem(position),position)
    }

    class ViewHolder(private val binding: VegiCardViewBinding, private val viewModel: AdminViewModel): RecyclerView.ViewHolder(binding.root){

        fun bind(item: LVegitables, position: Int){
            binding.vegiName.text=item.name
            binding.vegiCard.setOnClickListener {
                viewModel.getVegiId(position)
                it.findNavController().navigate(R.id.action_addItemFragment_to_subAddFragment)
            }
        }


    }

}