package com.example.sabzee3.admin.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sabzee3.R
import com.example.sabzee3.admin.viewmodel.AdminViewModel
import com.example.sabzee3.databinding.VegiCardViewBinding

class SubAdaptor(private val viewModel: AdminViewModel): ListAdapter<String, SubAdaptor.ViewHolder>(DiffCallBack) {

    companion object DiffCallBack:DiffUtil.ItemCallback<String>(){
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem==newItem
        }

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem==newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubAdaptor.ViewHolder {
        return ViewHolder(VegiCardViewBinding.inflate(LayoutInflater.from(parent.context)),viewModel)
    }

    override fun onBindViewHolder(holder: SubAdaptor.ViewHolder, position: Int) {
        holder.bind((getItem(position)),position)
    }

    class ViewHolder(private val binding: VegiCardViewBinding,private val viewModel: AdminViewModel): RecyclerView.ViewHolder(binding.root){
        fun bind(item: String, position: Int){
            binding.vegiName.text = item
            binding.vegiCard.setOnClickListener {
                viewModel.getVegiSubId(position)
                it.findNavController().navigate(R.id.action_subAddFragment_to_addVegiFragment)
            }
        }
    }
}