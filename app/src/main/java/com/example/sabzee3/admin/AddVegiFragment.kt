package com.example.sabzee3.admin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.sabzee3.R
import com.example.sabzee3.Vegetables
import com.example.sabzee3.admin.viewmodel.AdminViewModel
import com.example.sabzee3.databinding.FragmentAddVegiBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AddVegiFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddVegiFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentAddVegiBinding
    private lateinit var viewModel: AdminViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentAddVegiBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(requireActivity()).get(AdminViewModel::class.java)
        binding.vegiName.text = viewModel.vegiName()
        binding.vegiSubName.text = viewModel.vegiSubName()
        binding.saveButton.setOnClickListener {
            viewModel.saveToFirestore(Vegetables(viewModel.vegiName(),viewModel.vegiSubName(),binding.editTextNumber.text.toString().toInt(),binding.editTextNumber2.text.toString().toInt()))
        }
        viewModel.vegiSaved.observe(viewLifecycleOwner, Observer {
            it?.let {
                if(it){
                    Toast.makeText(context,"Sucessful",Toast.LENGTH_SHORT).show()
                }
            }
        })
        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AddVegiFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AddVegiFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}