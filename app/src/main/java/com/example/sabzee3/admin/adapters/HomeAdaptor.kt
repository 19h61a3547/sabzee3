package com.example.sabzee3.admin.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sabzee3.Vegetables
import com.example.sabzee3.databinding.FragmentAddVegiBinding
import com.example.sabzee3.databinding.HomeCardViewBinding

class HomeAdaptor :ListAdapter<Vegetables, HomeAdaptor.ViewHolder>(DiffCallBack) {

    companion object DiffCallBack:DiffUtil.ItemCallback<Vegetables>(){
        override fun areItemsTheSame(oldItem: Vegetables, newItem: Vegetables): Boolean {
            return oldItem.name==newItem.name
        }

        override fun areContentsTheSame(oldItem: Vegetables, newItem: Vegetables): Boolean {
            return oldItem==newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeAdaptor.ViewHolder {
        return ViewHolder(HomeCardViewBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: HomeAdaptor.ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder(val binding: HomeCardViewBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(item : Vegetables){
            binding.vegiNameHome.text = item.name
            binding.vegiSubNameHome.text = item.subname
            binding.vegiCostHome.text = "Cost : ${item.cost}"
            binding.vegiQuantityHome.text = "Quantity: ${item.quantity}"
        }
    }
}